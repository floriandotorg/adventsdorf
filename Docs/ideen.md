Verbindung zur LED
==================

![Verbinder](BL_2X10W_2_00.jpg)
Daran wird eine Platine gelötet, welches zusammen damit im Loch steckt und mit der Hausplatine verbunden ist. Darein werden die LEDs gesteckt und dann darum herum ein 3d-gedrucktes Gehäuse zum stabilisieren.

Wenn man dann noch Langlöcher einbaut, sodass das 3D gedruckte Teil in ein Langloch in der Platine greift, wäre die Buchse sogar höhenverstellbar also jedem Loch anpassbar!