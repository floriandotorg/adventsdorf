## Acces via Webbrowser
http://adventsdorf.local

## Instructions for OTA Upload 
In the platformio.ini uncomment the following lines:

    upload_port = Adventsdorf
    upload_flags = --auth=freddel
    upload_protocol = espota

Open Platformio menu and click "Upload" or "Upload an Monitor" to upload over the air. Click "Upload Filesystem Image OTA" to upload the filesystem over the air.
