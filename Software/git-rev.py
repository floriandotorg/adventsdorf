from subprocess import run, PIPE
from datetime import datetime

result = run("git rev-parse HEAD", stdout=PIPE, stderr=PIPE)
commit_hash = result.stdout.decode("ascii")[:7]
print(f"-DGIT_REV='{commit_hash}'")

result = run("git tag --points-at", stdout=PIPE, stderr=PIPE)
tag_names = result.stdout.decode("utf8").splitlines()

result = run("git status -s -- data src", stdout=PIPE, stderr=PIPE)
#result = run("git status -s -- data", stdout=PIPE, stderr=PIPE)
if result.stdout:
    print("-DGIT_MODIFIED_FILES")

ver = commit_hash
options = []
options += tag_names
if result.stdout:
    options += ["modified"]
options += [datetime.utcnow().strftime("%Y-%m-%d %H:%MZ")]
options = ", ".join(options)
if options:
    ver = f"{ver} ({options})"
print(f"-DVER='\"{ver}\"'")