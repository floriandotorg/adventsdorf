/**
 * @file Static_Functions.h
 * @author Julian Neundorf
 * @brief Consists of functions which are usable in multiple other files
 * @version 0.1
 * @date 2021-08-29
 *
 * Remarks:
 */

#pragma once

#include <Arduino.h>

 /**
  * @brief Clears the serial input buffer
  */
static void ClearSerial() {
  while (Serial.read() != -1)
    yield();
}

/**
* @brief Reads a single char from Serial and returns it
*
* @param blocking If true, the function is waiting until at least one char is received
* @param clear If true, the Serial input buffer is cleared after reading the first char
* @retval First read char, if no char is send returns -1
*/
static const char ReadSerialChar(const bool blocking = true, const bool clear = true) {
  while (blocking && !Serial.available())
    yield();
  const char c = Serial.read();
  if (clear)
    ClearSerial();
  return c;
}

/**
 * @brief Converts a 2 byte hex-string to a uint8_t
 *
 * @param hex 2 bytes hex string
 * @retval uint8_t Converted integer
 */
static uint8_t HexToInt(const char* hex) {
  uint8_t val = hex[0] >= '0' && hex[0] <= '9' ? hex[0] - '0' : (hex[0] >= 'a' && hex[0] <= 'f' ? hex[0] - 'a' + 10 : 0);
  val <<= 4;
  val += hex[1] >= '0' && hex[1] <= '9' ? hex[1] - '0' : (hex[1] >= 'a' && hex[1] <= 'f' ? hex[1] - 'a' + 10 : 0);
  return val;
}

/**
 * @brief Converts a String to a uint16_t
 *
 * @param str String with the integer
 * @retval uint8_t Converted integer
 */
static uint16_t StringToInt(String str) {
  uint16_t val = 0;
  for (uint8_t i = 0; i < str.length(); i++)
    val = val * 10 + (str[i] - '0');
  return val;
}

