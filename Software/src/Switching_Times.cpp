/**
 * @file Switching_Times.cpp
 * @author Julian Neundorf (julian.neundorf@tuhh.de)
 * @brief
 * @version 0.1
 * @date 2021-08-31
 *
 * Remarks:
 */

#include "Switching_Times.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "Classes.h"

SwitchingTimes::SwitchingTimes() {
  Load();
}

void SwitchingTimes::Load() {
  const uint8_t* load_times = *(storage->Switching_Times);
  Serial.println("Loading switching times:");

  for (uint8_t i = 0; i < MAX_SWICHTING_TIMES; i++) {
    uint8_t activate_hour = load_times[i * 6 + 0];
    uint8_t activate_minute = load_times[i * 6 + 1];
    uint8_t deactivate_hour = load_times[i * 6 + 2];
    uint8_t deactivate_minute = load_times[i * 6 + 3];
    uint8_t weekdays = load_times[i * 6 + 4];
    bool enabled = load_times[i * 6 + 5];
    if (activate_hour > 23 || activate_minute > 59 || deactivate_hour > 23 || deactivate_minute > 59) {
      Serial.printf("Warning! The loaded switching time (%u) is corrupt! %02u:%02u to %02u:%02u with weekdays %u!\n", i, activate_hour, activate_minute, deactivate_hour, deactivate_minute, weekdays);
    } else {
      times[i] = SwitchingTime(activate_hour, activate_minute, deactivate_hour, deactivate_minute, weekdays, enabled);
      if (enabled)
        Serial.printf(" %u: %02u:%02u to %02u:%02u with weekdays %u (%s)!\n", i, activate_hour, activate_minute, deactivate_hour, deactivate_minute, weekdays, enabled ? "Enabled" : "Disabled");
    }
  }
}

bool SwitchingTimes::GetSwitchingTime(uint8_t index, Time* activate, Time* deactivate, uint8_t* weekdays, bool* enabled) const {
  if (index >= MAX_SWICHTING_TIMES)
    return false;

  *activate = times[index].time_activate;
  *deactivate = times[index].time_deactivate;
  *weekdays = times[index].weekdays;
  *enabled = times[index].enabled;

  return true;
}

bool SwitchingTimes::SetSwitchingTime(uint8_t index, String activate, String deactivate, uint8_t weekdays, bool enabled) {
  if (index >= MAX_SWICHTING_TIMES)
    return false;

  times[index].time_activate = Time(activate);
  times[index].time_deactivate = Time(deactivate);
  times[index].weekdays = weekdays;
  times[index].enabled = enabled;

  char str[6] = { times[index].time_activate.Hour, times[index].time_activate.Minute, times[index].time_deactivate.Hour, times[index].time_deactivate.Minute, times[index].weekdays, times[index].enabled };
  Serial.printf("Save switching time (%u): %02u:%02u -> %02u:%02u an %u (%s)\n", index, str[0], str[1], str[2], str[3], str[4], str[5] ? "Enabled" : "Disabled");
  storage->Switching_Times->Save(str, 6, index * 6);
  return true;
}

bool SwitchingTimes::CheckTime() const {
  for (uint_fast16_t i = 0; i < MAX_SWICHTING_TIMES; i++) {
    const SwitchingTime& st = times[i];

    if (st.enabled && (st.weekdays & (1 << ((main_time.tm_wday + 6) % 7)))) { // Check if enabled and weekday fits
      if ((st.time_activate.Hour < main_time.tm_hour || (st.time_activate.Hour == main_time.tm_hour && st.time_activate.Minute <= main_time.tm_min))
        && (st.time_deactivate.Hour > main_time.tm_hour || (st.time_deactivate.Hour == main_time.tm_hour && st.time_deactivate.Minute > main_time.tm_min))) {
        return true;
      }
    }
  }

  return false;
}

// SwitchingTimes::SwitchingTime

SwitchingTimes::SwitchingTime::SwitchingTime() : time_activate(Time()), time_deactivate(Time()), weekdays(0), enabled(false) {}

SwitchingTimes::SwitchingTime::SwitchingTime(uint8_t _activate_Hour, uint8_t _activate_Minute, uint8_t _deactivate_Hour, uint8_t _deactivate_Minute, uint8_t _weekdays, bool _enabled) : time_activate(Time(_activate_Hour, _activate_Minute)), time_deactivate(Time(_deactivate_Hour, _deactivate_Minute)), weekdays(_weekdays), enabled(_enabled) {}


// SwitchingTimes::Time

SwitchingTimes::Time::Time() : Hour(0), Minute(0) {}

SwitchingTimes::Time::Time(uint8_t _hour, uint8_t _minute) : Hour(_hour), Minute(_minute) {}

SwitchingTimes::Time::Time(String _time) : Hour((_time[0] - '0') * 10 + _time[1] - '0'), Minute((_time[3] - '0') * 10 + _time[4] - '0') {
  if (Hour > 23)
    Hour = 0;
  if (Minute > 59)
    Minute = 0;
}

