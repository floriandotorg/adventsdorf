/**
 * @file Windmill.cpp
 * @author Julian Neundorf (julian.neundorf@tuhh.de)
 * @brief
 * @version 0.1
 * @date 2021-09-14
 *
 * Remarks:
*/

#include "Windmill.h"
#include "Classes.h"
#include "Arduino.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include <HTTPClient.h>
#include <ArduinoJson.h>
#include "StaticFunctions.h"

// Handler for swichting lights during test
TaskHandle_t Windmill_Handler;

/**
 * @brief Construct a new Windmill
*/
Windmill::Windmill() : auto_on(false), weather_app_id(url + 64), weather_city_id(url + 50) {
  const void* windmill_ptr = *(storage->Windmill);
  *((Windmill_Storage*)this) = *((Windmill_Storage*)windmill_ptr);

  for (uint8_t i = 0; i < 16; i++) {
    sprintf(weather_app_id + i * 2, "%x", weather_app_id_raw[i] >> 4);
    sprintf(weather_app_id + i * 2 + 1, "%x", weather_app_id_raw[i] % 16);
  }
  weather_app_id[32] = '&';

  sprintf(weather_city_id, "%07u", weather_city_id_raw);
  weather_city_id[7] = '&';

  Serial.printf("Saved Windmill configurations:\n Inputpin: %u\n Outputpin: %u\n Action_Duration: %u s\n Weather enabled: %s\n Weather App ID: %.32s\n Weather City ID: %.7s\n Wind_Speed_Trigger: %u\n URL: '%s'\n", inputpin, outputpin, action_duration, weather_enabled ? "Enabled" : "Disabled", weather_app_id, weather_city_id, weather_wind_speed_trigger, url);

  pinMode(inputpin, INPUT);
  pinMode(outputpin, OUTPUT);

  xTaskCreatePinnedToCore(Runner_static, "Windmill", 5000, this, 1, &Windmill_Handler, 1);
}

/**
 * @brief Caller function for Runner() to get called by xTask
 *
 * @param _this The windmill-object
*/
void Windmill::Runner_static(void* _this) {
  ((Windmill*)_this)->Runner();
}

/**
 * @brief Checks motion detection and de-/activates windmill
*/
void Windmill::Runner() {
  while (1) {
    switch (mode) {
    case Mode::On:
      digitalWrite(outputpin, HIGH);
      break;
    case Mode::Off:
      digitalWrite(outputpin, LOW);
      break;
    case Mode::Auto:
      if (weather_enabled && next_weather_check < millis() && WiFi.status() == WL_CONNECTED) {
        HTTPClient http;
        http.begin(url);
        int httpCode = http.GET();
        if (httpCode > 0) {
          String payload = http.getString();
          Serial.print("Response: '");
          Serial.print(payload);
          Serial.println("'");
          DynamicJsonDocument doc(1024);
          deserializeJson(doc, payload);
          weather_wind_speed = ((float)doc["wind"]["speed"]);
          next_weather_check = millis() + WEATHER_CHECK_WAITING_TIME;
          Serial.printf("Loaded windspeed: %u m/s\n", weather_wind_speed);
        } else {
          Serial.println("Wetterdaten konnten nicht geladen werden!");
        }
        http.end();
      }

      if (digitalRead(inputpin)) { // Motion detected
        if (!weather_enabled || weather_wind_speed > weather_wind_speed_trigger) {
          if (!auto_on)
            Serial.printf("%lu: Motion detected! Windmill activated!\n", millis());
          auto_on = true;
          digitalWrite(outputpin, HIGH);
          last_motion_triggered_time = millis();
        } else {
          if (last_motion_triggered_time + 5000 < millis())
            Serial.printf("%lu: Motion detected! Wind speed (%u m/s) too low!\n", millis(), weather_wind_speed);
          last_motion_triggered_time = millis();
        }
      } else {
        if (auto_on) {
          if (last_motion_triggered_time + 1000 * (action_duration - 2) < millis()) { // The motion sensor sends a 2 seconds signal
            Serial.printf("%lu: Switch windmill deactivated!\n", millis());
            auto_on = false;
            digitalWrite(outputpin, LOW);
          }
        }
      }
      break;
    }

    vTaskDelay(100 / portTICK_PERIOD_MS);
  }
}

/**
 * @brief Returns if the live weather feature is active
 *
 * @retval True, if live weather is used
*/
bool Windmill::GetLiveWeatherActive() const {
  return weather_enabled;
}

/**
 * @brief Return the trigger windspeed in m/s
 *
 * @retval uint8_t windspeed in m/s
*/
uint8_t Windmill::GetWindSpeedTrigger() const {
  return weather_wind_speed_trigger;
}

/**
 * @brief Get the city id defined by openweathermap.org
 *
 * @retval const char* city id
*/
const char* Windmill::GetCityID() const {
  return weather_city_id;
}

/**
 * @brief Get the appid defined by openweathermap.org
 *
 * @retval const char* app id
*/
const char* Windmill::GetAppID() const {
  return weather_app_id;
}

/**
 * @brief Get the pin of the windmill
 *
 * @retval uint8_t Pin of windmill
*/
uint8_t Windmill::GetPinWindmill() const {
  return outputpin;
}

/**
 * @brief Get the pin of the motion detector
 *
 * @retval uint8_t Pin of motion detector
*/
uint8_t Windmill::GetPinMotiondetector() const {
  return inputpin;
}

/**
 * @brief Get duration of a windmill action in seconds
 *
 * @retval uint16_t Duration of a windmill action in seconds
*/
uint16_t Windmill::GetActionduration() const {
  return action_duration;
}

/**
 * @brief Set the Configuration object
 *
 * @param _outputpin Outputpin for the windmill
 * @param _inputpin Inputput for the motion detector
 * @param _action_duration Action duration in seconds after triggering
 * @param _weather_enabled Enable triggering by live wind
 * @param _city_id City id of openweathermap.org for live wind
 * @param _app_id City id of openweathermap.org for live wind
 * @param _weather_wind_speed_trigger Least wind speed the windmill starts to run
 * @param _mode Mode (Off, Auto, On)
 * @retval Returns true if configuration could saved correctly
*/
bool Windmill::SetConfiguration(uint8_t _outputpin, uint8_t _inputpin, uint16_t _action_duration, bool _weather_enabled, const char* _app_id, const char* _city_id, uint8_t _weather_wind_speed_trigger, uint8_t _mode) {
  pinMode(outputpin, INPUT);
  outputpin = _outputpin;
  pinMode(outputpin, OUTPUT);
  inputpin = _inputpin;
  pinMode(inputpin, INPUT);
  action_duration = _action_duration;
  weather_enabled = _weather_enabled;
  memcpy(weather_app_id, _app_id, 32);
  memcpy(weather_city_id, _city_id, 7);
  weather_city_id[7] = '&';
  weather_wind_speed_trigger = _weather_wind_speed_trigger;
  mode = (Mode)_mode;

  for (uint8_t i = 0; i < 16; i++)
    weather_app_id_raw[i] = HexToInt(_app_id + 2 * i);

  sscanf(_city_id, "%u", &weather_city_id_raw);

  storage->Windmill->Save((Windmill_Storage*)this);

  Serial.printf("Saved Windmill configurations:\n Inputpin: %u\n Outputpin: %u\n Action_Duration: %u s\n Weather: %s\n Weather App ID: %.32s\n Weather City ID: %.7s\n Wind_Speed_Trigger: %u\n URL: '%s'\n", inputpin, outputpin, action_duration, weather_enabled ? "Enabled" : "Disabled", weather_app_id, weather_city_id, weather_wind_speed_trigger, url);

  return true;
}

/**
 * @brief Switches windmill on
*/
void Windmill::change_mode_on() {
  mode = Mode::On;
}

/**
 * @brief Switches windmill off
*/
void Windmill::change_mode_off() {
  mode = Mode::Off;
}

/**
 * @brief Switches windmill on if off and off if on
*/
void Windmill::change_mode_auto() {
  mode = Mode::Auto;
}

/**
 * @brief Get the Mode (Off, Auto, On)
 * @retval Mode
*/
Windmill::Mode Windmill::getMode() const {
  return mode;
}