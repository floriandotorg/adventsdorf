#include "webserver.h"
#include "Storage.h"
#include "FS.h"     // https://github.com/me-no-dev/arduino-esp32fs-plugin/releases/
#include <EEPROM.h>
#include "LED.h"
#include "Switching_Times.h"
#include "Switch.h"
#include "Classes.h"

void setup(void) {
  Serial.begin(500000);
  while (!Serial) taskYIELD();
  Serial.println("\n\nAdventsdorf");
  #ifdef VER
  Serial.println(VER);
  #else
  Serial.println("Unknown version");
  #endif

  Init();
}

void loop(void) {
  wifi->Check_Wifi();

  if (Serial.available()) {
    char c = Serial.read();
    Serial.readString();

    if (c == 'E') { // Reads EEPROM and prints to console
      Serial.print("       ");
      for (uint8_t i = 0; i < 16; i++)
        Serial.printf("0x*%x ", i);
      Serial.println();
      for (uint16_t i = 0; i < storage->EEPROM_SIZE / 16; i++) {
        Serial.printf("0x%02x*  ", i);
        for (uint8_t j = 0; j < 16; j++)
          Serial.printf("0x%02x ", (uint8_t)EEPROM.read(i * 16 + j));
        Serial.println();
      }
    }
  }
  taskYIELD();
}
