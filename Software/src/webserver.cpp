#include "webserver.h"
#include <WiFi.h>
#include <WiFiClient.h>
#include <WebServer.h>
#include "SPIFFS.h"            // https://github.com/me-no-dev/arduino-esp32fs-plugin/releases/
#include "StaticFunctions.h"
#include "time.h"
#include "Switch.h"
#include "Classes.h"
#include "ArduinoOTA.h"

// NTP-Zeit
#define NTPSERVERNAME "de.pool.ntp.org"
#define GMTOFFSET_SEC 3600
#define DAYLIGHTOFFSET_SEC 3600

const uint8_t WAIT_TIME_SECOND_WIFI_TRY = 3; // in Seconds
const uint8_t WAIT_TIME_WIFI_AP = 20;        // in Seconds

AsyncWebServer server(80);

int32_t SignalStrength;
TaskHandle_t Signal_Strength_Handler;

uint16_t village_coords[24][2] = { {84, 73}, {34, 7}, {6, 77}, {137, 5}, {1, 29}, {4, 54}, {127, 78}, {49, 73}, {74, 1}, {10, 7}, {24, 42}, {96, 2}, {135, 31}, {137, 57}, {102, 73}, {53, 2}, {107, 29}, {115, 3}, {66, 73}, {58, 51}, {87, 53}, {31, 72}, {119, 53}, {70, 30} };

WIFI::WIFI() {
  // Mainpage
  server.on("/", HTTP_GET, HandleRoot);
  server.on("/favicon.ico", HTTP_GET, [](AsyncWebServerRequest* request) {
    request->send(SPIFFS, "/favicon.png", "image/png");
    });
  server.on("/scripts.js", HTTP_GET, [](AsyncWebServerRequest* request) {
    request->send(SPIFFS, "/scripts.js", "text/javascript");
    });
  server.on("/style.css", HTTP_GET, [](AsyncWebServerRequest* request) {
    request->send(SPIFFS, "/style.css", "text/css");
    });

  // Lamptest
  server.on("/SingleLampTest", HTTP_GET, Handle_SingleTest);
  server.on("/CompleteLampTest", HTTP_GET, Handle_CompleteLampTest);
  server.on("/CycleLampTestTime", HTTP_GET, Handle_CycleLampTestTime);
  server.on("/CycleLampTestSwitch", HTTP_GET, Handle_CycleLampTestSwitch);
  server.on("/TestStop", HTTP_GET, Handle_StopTest);
  server.on("/lampstatus", HTTP_GET, [](AsyncWebServerRequest* request) {
    char lampstatus[26];
    for (uint8_t i = 0; i < 25; i++)
      lampstatus[i] = LED_control->GetLighton(i) ? '1' : '0';
    lampstatus[24] = LED_control->Getlamptesttype() != Lampteststatus::Off ? '1' : '0';
    lampstatus[25] = '\0';
    request->send(200, "text/html", lampstatus);
    });

  // Switching Times
  server.on("/load_switch_time", HTTP_GET, Handle_GetSwitchingTimes);
  server.on("/save_switch_time", HTTP_GET, Handle_SetSwitchingTimes);

  // Colors
  server.on("/reset_house_colors", HTTP_GET, Handle_ResetHouseColors);
  server.on("/try_color", HTTP_GET, Handle_TryColor);
  server.on("/set_house_colors_single", HTTP_GET, Handle_SetHouseColorsSingle);
  server.on("/set_house_colors_common", HTTP_GET, Handle_SetHouseColorsCommon);
  server.on("/get_house_color", HTTP_GET, Handle_GetHouseColor);
  server.on("/save_color_configuration", HTTP_GET, Handle_SetColorConfigurations);

  // Settings
  server.on("/save_switch_configuration", HTTP_GET, Handle_SetSwitchingConfiguration);

  server.on("/new_order", HTTP_GET, Handle_NewHouseOrdner);
  server.on("/reset_house_pos", HTTP_GET, Handle_ResetHousePos);

  server.on("/load_configuration_variable", HTTP_GET, Handle_LoadConfigurationVariable);
  server.on("/save_configuration_variable", HTTP_GET, Handle_SaveConfigurationVariable);

  server.on("/reboot", HTTP_GET, [](AsyncWebServerRequest* request) {
    Serial.println("Restarting...");
    request->redirect("/");
    WiFi.disconnect(true);
    ESP.restart();
    });
  server.on("/reset", HTTP_GET, Handle_Reset);

  // Switches
  server.on("/load_switch_config", HTTP_GET, Handle_LoadSwitchConfiguration);
  server.on("/save_switch", HTTP_GET, Handle_SaveSwitch);

  server.on("/418", HTTP_GET, [](AsyncWebServerRequest* request) {
    request->send(418, "text/plain", "418 I'm a teapot");
    });

  // Windmill
  server.on("/save_windmill_configuration", HTTP_GET, Handle_SaveWindmillConfiguration);

  server.onNotFound(HandleNotFound);

  xTaskCreatePinnedToCore(CalcSignalStrength, "Calc Signal Strength", 1000, NULL, 1, &Signal_Strength_Handler, 1);
}

void WIFI::Check_Wifi() {
  ArduinoOTA.handle();

  if (WiFi.status() == WL_CONNECTED) {
    if (!getLocalTime(&main_time)) {
      Serial.println("Failed to obtain time");
      return;
    }

    return;
  }

  Serial.println("No WiFi connection, try to connect!");

  Serial.println("Loading Wifi credentials from EEPROM...");
  const char* wifi_ssid = *(storage->WiFi_SSID);
  const char* wifi_pass = *(storage->WiFi_PASS);

  Serial.printf("Loaded WIFI-credentials:\nSSID: '%s'\nPassword: '%s'\n", wifi_ssid, wifi_pass);
  WiFi.begin(wifi_ssid, wifi_pass);
  WiFi.reconnect();
  Serial.print("Connecting WIFI...");
  uint8_t i = 0;
  while (WiFi.status() != WL_CONNECTED) {
    vTaskDelay(500 / portTICK_PERIOD_MS);
    Serial.print(".");
    i++;
    if (i == WAIT_TIME_SECOND_WIFI_TRY * 3 || WiFi.status() == WL_CONNECT_FAILED) { // 2s gone -> 2nd try
      Serial.printf("\nCould not connect to WIFI (SSID: %s), Resetting WiFi-Connecting and try one more time\n", wifi_ssid);
      WiFi.disconnect(true);
      vTaskDelay(1000 / portTICK_PERIOD_MS);
      WiFi.begin(wifi_ssid, wifi_pass);
    } else if (i > WAIT_TIME_WIFI_AP * 2) { // Wifi totally failed!
      StartAP();
      i = 0;
    }
  }

  Serial.print("\nIP number assigned by DHCP is ");
  Serial.println(WiFi.localIP());

  server.begin();
  SignalStrength = WiFi.RSSI();

  ArduinoOTA.setHostname("adventsdorf");
  ArduinoOTA.setPassword("freddel");
  ArduinoOTA.begin();

  // Zeit
  configTime(GMTOFFSET_SEC, DAYLIGHTOFFSET_SEC, NTPSERVERNAME);
  if (!getLocalTime(&main_time)) {
    Serial.println("Failed to obtain time");
    return;
  }
  Serial.println(&main_time, "%A, %B %d %Y %H:%M:%S");

  Serial.printf("Adventsdorf zugreifbar über: adventsdorf.local\n");
}

uint8_t WIFI::GetSignalStrengthPercentage() {
  if (SignalStrength < -100)
    return 0;
  else if (SignalStrength > -50)
    return 100;
  else
    return (uint8_t)((SignalStrength + 100) * 2);
}

void WIFI::CalcSignalStrength(void* pvParameters) {
  while (1) {
    SignalStrength += WiFi.RSSI();
    SignalStrength /= 2;
    vTaskDelay(1000 / portTICK_PERIOD_MS);
  }
}

// ============================== Handles ==============================
// ============================== General ==============================

void WIFI::HandleRoot(AsyncWebServerRequest* request) {
  request->send(SPIFFS, "/index.html", "text/html", false, HandleRoot_Placeholder);
}

String WIFI::HandleRoot_Placeholder(const String& var) {
  if (var == "VILLAGE") {
    const uint8_t* house_pos = *(storage->House_Positions);

    char* wp = (char*)calloc(10000, sizeof(char));
    char* wp2 = wp;

    const char mainpage1[] PROGMEM = R"=====(
<div id="ziel%s%d" class="target" ondrop="drop(event)" ondragover="allowDrop(event)" onclick="clickhouse('%u')" style="top: %dpx; left: %dpx;">
  <div id="Haus%s%d" class="house%s" draggable="true" ondragstart="drag(event)"><div><div></div></div><b>%d</b></div>
</div>
)=====";
    for (uint_fast8_t i = 1; i < 25; i++) {
      sprintf(wp2, mainpage1, i < 10 ? "0" : "", i, i, village_coords[house_pos[i - 1]][1] * 5, village_coords[house_pos[i - 1]][0] * 5, i < 10 ? "0" : "", i, LED_control->GetLighton(i) ? " house_on" : "", i);
      wp2 += strlen(wp2);
    }
    String str = String(wp);
    free(wp);
    return str;
  } else if (var == "SSID") {
    return WiFi.SSID();
  } else if (var == "WIFI_POWER") {
    char str[20];
    sprintf(str, "%u%%%% (%d dB)", GetSignalStrengthPercentage(), SignalStrength);
    return String(str);
  } else if (var == "CONFIGURATION_MODE") {
    String str;
    LED_Control::Mode mode = LED_control->GetMode();
    const char* names[3] = { "Aus", "Auto", "An" };
    for (uint8_t i = 0; i < 3; i++) {
      str += "<option value=\"";
      str += i;
      str += "\"";
      if (mode == i)
        str += " selected=\"selected\"";
      str += ">";
      str += names[i];
      str += "</option>\n";
    }
    return str;
  } else if (var == "WINDMILL_MODE") {
    String str;
    uint8_t mode = (uint8_t)windmill->getMode();
    const char* names[3] = { "Aus", "Auto", "An" };
    for (uint8_t i = 0; i < 3; i++) {
      str += "<option value=\"";
      str += i;
      str += "\"";
      if (mode == i)
        str += " selected=\"selected\"";
      str += ">";
      str += names[i];
      str += "</option>\n";
    }
    return str;
  } else if (var == "CONFIGURATION_SYLVESTER") {
    return String(LED_control->GetSylvester() ? " checked" : "");
  } else if (var == "CONFIGURATION_DEBUG") {
    return String(LED_control->GetDebug() ? " checked" : "");
  } else if (var == "BRIGHTNESS") {
    return String((uint8_t) * (storage->Brightness));
  } else if (var == "BRIGHTNESS_P") {
    return String(*(storage->Brightness) * 100 / 255);
  } else if (var == "SWITCH_NAMES") {
    uint8_t Am_Switches = switches->GetAmountSwitches();
    const char** names = switches->GetNames();
    String str = "";
    for (uint8_t i = 0; i < Am_Switches; i++) {
      str += "<option value=\"";
      str += i;
      str += "\"> ";
      str += names[i];
      str += "</option>";
    }
    free(names);
    return str;
  } else if (var == "EEPROM_NAMES") {
    String str = "";
    for (uint8_t i = 0; i < storage->OBJECTS_COUNT; i++) {
      const Storage::Object* obj = storage->GetObject(i);
      if (obj == nullptr)
        continue;
      str += "<option value=\"";
      str += String(i);
      str += "\">";
      str += obj->GetName();
      str += "</option>";
    }
    return str;
  } else if (var == "SWITCH_FUNCTIONS") {
    const char* function_names[] = {
      "Keine",
      "Modus: Aus", "Modus: Auto", "Modus: An",
      "Lichttest beenden", "Lichttest: Komplett", "Lichttest: Zeitzyklus", "Lichttest: Klickzyklus",
      "Windmuehle: Aus", "Windmuehle: Auto", "Windmuehle: Ein" };

    String str;
    for (uint8_t i = 0; i < 6; i++) {
      str += "<tr id=\"switch_fun_" + String(i) + "_row\">\n";
      str += "<td><label for=\"switch_fun_" + String(i) + "\" id=\"switch_fun_" + String(i) + "_label\"></label></td>";
      str += "<td><select id=\"switch_fun_" + String(i) + "\" name=\"switch_fun_" + String(i) + "\">";
      for (uint8_t j = 0; j < Switches::AM_AVAILABLE_SWITCH_FUNS + 1; j++) {
        str += "<option value=\"" + String(j) + "\">" + function_names[j] + "</option>\n";
      }
      str += "</select></td>\n</tr>\n";
    }
    return str;
  } else if (var.substring(0, 4) == "PINS") {
    uint8_t pins[22] = { 2, 4, 5, 12, 13, 14, 15, 16, 17, 18, 19, 21, 22, 23, 25, 26, 27, 32, 33 }; // 0, 34 and 35 are special but theoretically connectable

    uint8_t selected = 0;
    if (var[4] == '1')
      selected = windmill->GetPinWindmill();
    else if (var[4] == '2')
      selected = windmill->GetPinMotiondetector();

    String str = "";
    for (uint8_t i = 0; i < 22; i++) {
      str += "<option value=\"";
      str += pins[i];
      str += "\"";
      if (pins[i] == selected)
        str += " selected=\"selected\"";
      str += ">";
      str += pins[i];
      str += "</option>\n";
    }
    return str;
  } else if (var == "WM_LIVEWEATHER_ACTIVE") {
    return String(windmill->GetLiveWeatherActive() ? " checked" : "");
  } else if (var == "WINDMILL_WS_TRG") {
    return String(windmill->GetWindSpeedTrigger());
  } else if (var == "WINDMILL_CITY_ID") {
    return String(windmill->GetCityID()).substring(0, 7);
  } else if (var == "WINDMILL_APP_ID") {
    return String(windmill->GetAppID()).substring(0, 32);
  } else if (var == "WINDMILL_ACT_DUR") {
    return String(windmill->GetActionduration());
  } else if (var == "IP") {
    return WiFi.localIP().toString();
  } else if (var == "VERSION") {
#ifdef VER
    return String(VER);
#else
    return String("Unknown");
#endif
  } else if (var == "DATETIME") {
    char datetime[20];
    sprintf(datetime, "%02d.%02d.%04d %02d:%02d:%02d", main_time.tm_mday, main_time.tm_mon + 1, main_time.tm_year + 1900, main_time.tm_hour, main_time.tm_min, main_time.tm_sec);
    return String(datetime);
  }
  return String();
}

void WIFI::HandleNotFound(AsyncWebServerRequest* request) {
  String message = "File Not Found\n\n";
  message += "URI: ";
  message += request->url();
  message += "\nMethod: ";
  message += (request->method() == HTTP_GET) ? "GET" : "POST";
  message += "\nArguments: ";
  message += request->args();
  message += "\n";

  for (uint8_t i = 0; i < request->args(); i++) {
    message += " " + request->argName(i) + ": " + request->arg(i) + "\n";
  }

  request->send(404, "text/plain", message);
}


// ============================== Lamptests ==============================

void WIFI::Handle_CompleteLampTest(AsyncWebServerRequest* request) {
  LED_control->SwitchLampTest(Lampteststatus::Complete);
  request->redirect("/");
}

void WIFI::Handle_CycleLampTestTime(AsyncWebServerRequest* request) {
  LED_control->SwitchLampTest(Lampteststatus::CycleTime);
  request->redirect("/");
}

void WIFI::Handle_CycleLampTestSwitch(AsyncWebServerRequest* request) {
  LED_control->SwitchLampTest(Lampteststatus::CycleSwitch, 0);
  request->redirect("/");
}

void WIFI::Handle_SingleTest(AsyncWebServerRequest* request) {
  if (request->args() > 0) {
    uint8_t lamp;
    String a = request->arg(0U);
    if (a.length() > 1) {
      lamp = 10 * (a[0] - '0') + (a[1] - '0');
    } else {
      lamp = a[0] - '0';
    }
    lamp--;
    Serial.printf("Teste Lampe: %u\n", lamp + 1);
    LED_control->SwitchLampTest(Lampteststatus::Single, lamp); // Das richtige Haus aktivieren!
  } else
    LED_control->SwitchLampTest(Lampteststatus::Single, -1);
  request->redirect("/");
}

void WIFI::Handle_StopTest(AsyncWebServerRequest* request) {
  LED_control->SwitchLampTest(Lampteststatus::Off);
  request->redirect("/");
}


// ============================== Switching Times ==============================

void WIFI::Handle_SetSwitchingConfiguration(AsyncWebServerRequest* request) {
  if (request->args() == 3) {
    String mode_str = request->arg(0U);
    String sylvester_str = request->arg(1U);
    String debug_str = request->arg(2U);

    LED_control->SetMode(mode_str[0] - '0');
    LED_control->SetSylvester(sylvester_str[0] == '1');
    LED_control->SetDebug(debug_str[0] == '1');

    LED_control->SaveConfiguration();

    request->send(200, "text/html", "0");
  } else {
    Serial.printf("Handle_SetSwitchingConfiguration: Too less arguments: %u/3 (URL: '%s')\n", request->args(), request->url().c_str());
    request->send(200, "text/html", "Falsche Anzahl Argumente!");
  }
}

void WIFI::Handle_GetSwitchingTimes(AsyncWebServerRequest* request) {
  if (request->args() == 1) {
    String index_str = request->arg(0U);
    uint8_t index = StringToInt(index_str);

    SwitchingTimes::Time activate;
    SwitchingTimes::Time deactivate;
    uint8_t weekdays;
    bool enabled;

    if (switchingtimes->GetSwitchingTime(index, &activate, &deactivate, &weekdays, &enabled)) {
      char wp[30];
      sprintf(wp, "0 %02u:%02u-%02u:%02u %03u %u", activate.Hour, activate.Minute, deactivate.Hour, deactivate.Minute, weekdays, enabled);
      request->send(200, "text/html", wp);
    } else { // Index wrong
      request->send(200, "text/html", "Wrong index!");
    }
  }
}

void WIFI::Handle_SetSwitchingTimes(AsyncWebServerRequest* request) {
  if (request->args() == 5) {
    String index_str = request->arg(0U);
    String activate_str = request->arg(1U);
    String deactivate_str = request->arg(2U);
    String weekdays_str = request->arg(3U);
    String enabled_str = request->arg(4U);

    uint8_t index = StringToInt(index_str);

    uint8_t weekday = weekdays_str[0] - '0';
    for (uint8_t i = 1; i < 3 && weekdays_str[i] >= '0' && weekdays_str[i] <= '9'; i++) {
      weekday *= 10;
      weekday += weekdays_str[i] - '0';
    }

    switchingtimes->SetSwitchingTime(index, activate_str, deactivate_str, weekday, enabled_str[0] != '0');
    request->send(200, "text/html", "0");
  } else {
    Serial.printf("Handle_SetSwitchingTimes: Too less arguments: %u/5 (URL: '%s')\n", request->args(), request->url().c_str());
    request->send(200, "text/html", "Falsche Anzahl Argumente!");
  }
}


// ============================== House Colors ==============================

void WIFI::Handle_TryColor(AsyncWebServerRequest* request) {
  if (request->args() == 2) {
    String lamp_str = request->arg(0U);
    String color_str = request->arg(1U);

    uint8_t color[3];
    for (uint8_t i = 0; i < 3; i++)
      color[i] = HexToInt(&color_str[2 * i]);

    if (lamp_str[0] == 'a')
      LED_control->SwitchLampTest(Lampteststatus::Complete, -1, color);
    else
      LED_control->SwitchLampTest(Lampteststatus::Single, StringToInt(lamp_str), color); // Das richtige Haus aktivieren!

    request->send(200, "text/html", "0");
  } else {
    Serial.printf("Handle_TryColor: Too less arguments: %u/2 (URL: '%s')\n", request->args(), request->url().c_str());
    request->send(200, "text/html", "Falsche Anzahl Argumente!");
  }
}

void WIFI::Handle_SetHouseColorsCommon(AsyncWebServerRequest* request) {
  if (request->args() == 2) {
    String color_str = request->arg(0U);
    String variance_str = request->arg(1U);

    uint8_t color[3];
    for (uint8_t i = 0; i < 3; i++)
      color[i] = HexToInt(&color_str[2 * i]);

    uint8_t variance = StringToInt(variance_str);

    uint8_t colors[24 * 3];
    for (uint8_t i = 0; i < 24; i++) {
      for (uint8_t j = 0; j < 3; j++) {
        uint16_t index = i * 3 + j;
        int16_t new_color = color[j] * (1 + variance / 100. * ((float)random8() - 128) / 128.);
        if (new_color > 255)
          colors[index] = 255;
        else if (new_color < 0)
          colors[index] = 0;
        else
          colors[index] = new_color;
      }
    }

    storage->House_Colors->Save(colors);
    LED_control->LoadColors();

    request->send(200, "text/html", "0");
  } else {
    Serial.printf("Handle_SetHouseColorsCommon: Too less arguments: %u/2 (URL: '%s')\n", request->args(), request->url().c_str());
    request->send(200, "text/html", "Falsche Anzahl Argumente!");
  }
}

void WIFI::Handle_SetHouseColorsSingle(AsyncWebServerRequest* request) {
  if (request->args() == 2) {
    String lamp_str = request->arg(0U);
    String color_str = request->arg(1U);

    uint8_t lamp = StringToInt(lamp_str);

    uint8_t color[3];
    for (uint8_t i = 0; i < 3; i++)
      color[i] = HexToInt(&color_str[2 * i]);

    if (lamp > 23) {
      Serial.printf("Handle_SetHouseColorsSingle: Wrong lamp id: %u (URL: '%s')\n", lamp, request->url().c_str());
      request->send(200, "text/html", "Hausnummer nicht vergeben!");
      return;
    }
    storage->House_Colors->Save(color, 3, 3 * lamp);
    LED_control->LoadColors();

    request->send(200, "text/html", "0");
  } else {
    Serial.printf("Handle_SetHouseColorsSingle: Too less arguments: %u/2 (URL: '%s')\n", request->args(), request->url().c_str());
    request->send(200, "text/html", "Falsche Anzahl Argumente!");
  }
}

void WIFI::Handle_GetHouseColor(AsyncWebServerRequest* request) {
  if (request->args() == 1) {
    String lamp_str = request->arg(0U);

    uint8_t lamp = 0;
    for (uint8_t i = 0; i < lamp_str.length(); i++)
      lamp = lamp * 10 + (lamp_str[i] - '0');

    if (lamp > 23) {
      Serial.printf("Handle_GetHouseColor: Wrong lamp id: %u (URL: '%s')\n", lamp, request->url().c_str());
      request->send(200, "text/html", "Hausnummer nicht vergeben!");
      return;
    }

    const CRGB* color = LED_control->GetHouseColors() + lamp;
    char response[10];
    snprintf(response, 9, "0 %02x%02x%02x", color->r, color->g, color->b);
    request->send(200, "text/html", response);
  } else {
    Serial.printf("Handle_GetHouseColor: Too less arguments: %u/1 (URL: '%s')\n", request->args(), request->url().c_str());
    request->send(200, "text/html", "Falsche Anzahl Argumente!");
  }
}

void WIFI::Handle_SetColorConfigurations(AsyncWebServerRequest* request) {
  if (request->args() == 1) {
    String brightness_str = request->arg(0U);
    uint8_t brightness = StringToInt(brightness_str);

    storage->Brightness->Save(brightness);
    Serial.printf("Set Color Configurations:\nBrightness: %u\n", brightness);
    LED_control->SetBrightness(brightness);
    request->send(200, "text/html", "0");
  } else {
    Serial.printf("Handle_GetHouseColor: Too less arguments: %u/1 (URL: '%s')\n", request->args(), request->url().c_str());
    request->send(200, "text/html", "Falsche Anzahl Argumente!");
  }
}

void WIFI::Handle_ResetHouseColors(AsyncWebServerRequest* request) {
  uint8_t colors[24 * 3];
  for (uint8_t i = 0; i < 24; i++) {
    colors[i * 3 + 0] = 255; // TODO generalize!
    colors[i * 3 + 1] = 221;
    colors[i * 3 + 2] = 104;
  }

  storage->House_Colors->Save(colors);
  LED_control->LoadColors();
  request->send(200, "text/html", "0");
}


// ============================== Configurations ==============================
void WIFI::Handle_NewHouseOrdner(AsyncWebServerRequest* request) {
  if (!request->hasArg("house_ids")) {
    Serial.println("house_ids == null!\n");
    request->send(200, "text/html", "house_ids = null!");
    return;
  }
  Serial.print("Arg: '");
  Serial.print(request->arg("house_ids"));
  Serial.print("'\n");
  const uint8_t* house_pos = *(storage->House_Positions);
  uint8_t new_house_pos[24];
  memcpy(new_house_pos, house_pos, 24);

  char* in = (char*)malloc(request->arg("house_ids").length() + 1);
  char* start = in;
  strcpy(in, request->arg("house_ids").c_str());
  while (in[0] >= '0' && in[0] <= '9' && in[1] >= '0' && in[1] <= '9' && in[2] >= '0' && in[2] <= '9' && in[3] >= '0' && in[3] <= '9') {
    uint8_t ziel = 10 * (in[0] - '0') + (in[1] - '0');
    ziel--;
    uint8_t ziel2 = 10 * (in[2] - '0') + (in[3] - '0');
    ziel2--;
    Serial.printf("Setze:\n%u -> %u\n%u -> %u\n", new_house_pos[ziel2], ziel, new_house_pos[ziel], ziel2);
    uint8_t buff = new_house_pos[ziel];
    new_house_pos[ziel] = new_house_pos[ziel2];
    new_house_pos[ziel2] = buff;
    in += 4;
    Serial.printf("Next Char: %c (%u)\n", in[0], in[0]);
  }
  free(start);
  storage->House_Positions->Save(new_house_pos);

  request->redirect("/");
}

void WIFI::Handle_ResetHousePos(AsyncWebServerRequest* request) {
  uint8_t pos[24];
  for (uint8_t i = 0; i < 24; i++)
    pos[i] = i;
  storage->House_Positions->Save(pos, 24);

  request->redirect("/");
}

void WIFI::Handle_LoadConfigurationVariable(AsyncWebServerRequest* request) {
  Serial.print("Lade Konfigurationsvariable ");
  if (request->args() > 0) {
    String a = request->arg(0U);
    uint8_t index = 0;
    for (uint8_t i = 0; i < a.length(); i++)
      index = index * 10 + (a[i] - '0');

    const Storage::Object* obj = storage->GetObject(index);
    if (obj == nullptr) {
      request->send(200, "text/html", "Konfiguration nicht gespeichert! Unbekannte Adresse!");
      return;
    }
    Serial.printf("'%s' (%u): ", obj->GetName(), index);
    const char* value = *obj;
    if (obj->GetType() == Storage::Object::Type::Text) {
      Serial.printf("'%s'\n", value);
      request->send(200, "text/html", value);
    } else {
      const uint16_t length = obj->GetLength();
      char output[length * 5 + 1];
      char* output_ptr = output;

      for (uint16_t i = 0; i < length; i++) {
        sprintf(output_ptr, "0x%02x ", value[i]);
        output_ptr += strlen(output_ptr);
      }
      *output_ptr = '\0';

      Serial.printf("'%s'\n", output);
      request->send(200, "text/html", output);
    }

    return;
  }
  request->send(200, "text/html", "Not found");
}

void WIFI::Handle_SaveConfigurationVariable(AsyncWebServerRequest* request) {
  if (request->args() == 2) {
    String name = request->arg(0U);
    String value = request->arg(1U);
    uint16_t length = value.length();

    uint8_t index = 0;
    for (uint8_t i = 0; i < name.length(); i++)
      index = index * 10 + (name[i] - '0');

    Storage::Object* obj = storage->GetObject(index);
    if (obj == nullptr) {
      request->send(200, "text/html", "Konfiguration nicht gespeichert! Unbekannte Adresse!");
      return;
    }

    Serial.printf("Save to configuration '%s' (%u) the value(s): '", obj->GetName(), index);
    Serial.print(value);
    Serial.println("'");

    if (obj->GetType() == Storage::Object::Type::Bytes) {
      length = value.length() / 5;

      Serial.print("Bytes: ");

      for (uint16_t i = 0; i < length; i++) {
        if (value[i * 5 + 2] >= '0' && value[i * 5 + 2] <= '9')
          value[i] = (value[i * 5 + 2] - '0') * 16;
        else
          value[i] = (value[i * 5 + 2] - 'a' + 10) * 16;

        if (value[i * 5 + 3] >= '0' && value[i * 5 + 3] <= '9')
          value[i] += value[i * 5 + 3] - '0';
        else
          value[i] += value[i * 5 + 3] - 'a' + 10;

        Serial.printf("%d ", value[i]);
      }
      Serial.println();
    }

    if (obj->Save(value.c_str(), length)) {
      request->send(200, "text/html", "Konfiguration nicht gespeichert! Unbekannte Adresse!");
    } else {
      request->send(200, "text/html", "Konfiguration gespeichert!");
    }
    return;
  }
  request->send(200, "text/html", "Falsche Anzahl Arugmente");
}

void WIFI::Handle_Reset(AsyncWebServerRequest* request) {
  if (request->args() > 0) {
    request->send(200, "text/html", "ESP-Einstellung wird zurückgesetzt");
    if (request->argName(0) == "id")
      storage->Reset(StringToInt(request->arg(0U)));
    else if (request->argName(0) == "wifi")
      storage->ResetAll(request->args() == 1 && request->arg(0U) == "true");
  } else {
    request->send(200, "text/html", "ESP wird zurückgesetzt");
    storage->ResetAll(false);
  }

  switchingtimes->Load();
  LED_control->LoadColors();
}


// ============================== Switches ==============================

void WIFI::Handle_LoadSwitchConfiguration(AsyncWebServerRequest* request) {
  if (request->args() == 1) {
    String index_str = request->arg(0U);
    uint8_t index = StringToInt(index_str);

    const char* name;
    uint8_t type;
    uint8_t pin;
    uint8_t pin2;
    const uint8_t* fun;

    if (switches->GetConfiguration(index, &name, &type, &pin, &pin2, &fun)) {
      char wp[60];
      sprintf(wp, "0 %s;%d;%d;%d;", name, type, pin, pin2);
      char* wp2 = wp + strlen(wp);
      for (uint8_t i = 0; i < 6; i++, wp2 += strlen(wp2))
        sprintf(wp2, "%d;", fun[i]);

      request->send(200, "text/html", wp);
    } else { // Index wrong
      request->send(200, "text/html", "Wrong index!");
    }
  }
}

void WIFI::Handle_SaveSwitch(AsyncWebServerRequest* request) {
  if (request->args() == 11) {
    uint8_t id = StringToInt(request->arg(0U));
    String name = request->arg(1U);
    char name_cc[20] = { '\0' };
    strncpy(name_cc, name.c_str(), name.length());
    uint8_t type = StringToInt(request->arg(2U));
    uint8_t pin = StringToInt(request->arg(3U));
    uint8_t pin2 = StringToInt(request->arg(4U));
    uint8_t fun[6];
    for (uint8_t i = 0; i < 6; i++)
      fun[i] = StringToInt(request->arg(i + 5));

    if (switches->SetConfiguration(id, name_cc, type, pin, pin2, fun))
      request->send(200, "text/html", "0");
    else {
      Serial.printf("Handle_SaveSwitch: ID falsch: %u\n", id);
      request->send(200, "text/html", "Schalter ID falsch!");
    }
  } else {
    Serial.printf("Handle_SaveSwitch: Too less arguments: %u/11 (URL: '%s')\n", request->args(), request->url().c_str());
    request->send(200, "text/html", "Falsche Anzahl Argumente!");
  }
}


// ============================== Windmill ==============================

void WIFI::Handle_SaveWindmillConfiguration(AsyncWebServerRequest* request) {
  if (request->args() == 8) {
    uint8_t outputpin = StringToInt(request->arg(0U));
    uint8_t inputpin = StringToInt(request->arg(1U));
    uint16_t action_duration = StringToInt(request->arg(2U));
    bool weather_enabled = request->arg(3U)[0] == '1';
    String app_id = request->arg(4U);
    String city_id = request->arg(5U);
    uint8_t wind_trigger = StringToInt(request->arg(6U));
    uint8_t mode = StringToInt(request->arg(7U));

    if (windmill->SetConfiguration(outputpin, inputpin, action_duration, weather_enabled, app_id.c_str(), city_id.c_str(), wind_trigger, mode))
      request->send(200, "text/html", "0");
    else {
      Serial.printf("Handle_SaveWindmillConfiguration: Windmühlenkonfiguration konnte nicht gespeichert werden!\n");
      request->send(200, "text/html", "Konfiguration konnte nicht gespeichert werden!");
    }
  } else {
    Serial.printf("Handle_SaveSwitch: Too less arguments: %u/7 (URL: '%s')\n", request->args(), request->url().c_str());
    request->send(200, "text/html", "Falsche Anzahl Argumente!");
  }
}



void WIFI::StartAP() {
  WiFi.softAP(WIFI_AP_SSID, WIFI_AP_PASS);
  IPAddress IP = WiFi.softAPIP();
  Serial.printf("\nSSID: %s\nPASS: %s", WIFI_AP_SSID, WIFI_AP_PASS);
  Serial.print("\nAP IP address: ");
  Serial.println(IP);
  Serial.printf("Alternativ WIFI-Daten über die serielle Schnittstelle angeben. Dazu 'y' senden.\n");
  WiFiServer server(80);
  server.begin();
  String header;
  while (1) {
    if (ReadSerialChar(false, true) == 'y') {
      char* wifi_ssid = (char*)calloc(33, sizeof(char));
      char* wifi_pass = (char*)calloc(64, sizeof(char));
      int16_t i = 0;
      Serial.printf("SSID:\n");
      wifi_ssid[i++] = ReadSerialChar(true, false);
      while (i < 32 && (wifi_ssid[i] = Serial.read()) != '\n') {
        if (wifi_ssid[i] == (uint8_t)-1)
          i--;
        i++;
        taskYIELD();
      }
      wifi_ssid[i] = '\0';
      i = 0;
      ClearSerial();
      Serial.printf("Passwort:\n");
      wifi_pass[i++] = ReadSerialChar(true, false);
      while (i < 63 && (wifi_pass[i] = Serial.read()) != '\n') {
        if (wifi_pass[i] == (uint8_t)-1)
          i--;
        i++;
        taskYIELD();
      }
      wifi_pass[i] = '\0';
      i = -1;

      Serial.printf("Eingelesene Daten:\nSSID: '%s' (%u Zeichen)\nPasswort: '%s' (%u Zeichen)\nSollen die Daten im EEPROM abgespeichert und der Computer neugestartet werden?\nBestätigen mit 'y'\n", wifi_ssid, strlen(wifi_ssid), wifi_pass, strlen(wifi_pass));
      ClearSerial();
      if (ReadSerialChar() == 'y') {
        storage->WiFi_SSID->Save(wifi_ssid);
        storage->WiFi_PASS->Save(wifi_pass);
        Serial.printf("WIFI-Daten gespeichert, starte ESP neu...\n");
        ESP.restart();
      }
    }
    WiFiClient client = server.available();
    if (client) { // If a new client connects,
      Serial.println("WIFI-Client connected!");
      String currentLine = "";
      while (client.connected()) {
        if (client.available()) { // if there's bytes to read from the client,
          char c = client.read();
          Serial.write(c);
          header += c;
          if (c == '\n') {
            if (currentLine.length() == 0) { // End of HTTP Request
              client.println("HTTP/1.1 200 OK");
              client.println("Content-type:text/html");
              client.println("Connection: close");
              client.println();
              int16_t i;
              char* wifi_ssid = (char*)calloc(33, sizeof(char));
              char* wifi_pass = (char*)calloc(64, sizeof(char));
              if ((i = header.indexOf("GET /new_storage.php?ssid=")) >= 0) {
                i += 30; // Length("GET /new_storage.php?ssid=")
                uint8_t j = 0;
                while (j < 32 && header[i] != '&') {
                  if (header[i] == '%') {
                    wifi_ssid[j] = (header[i + 1] - '0') * 16 + (header[i + 2] > 64 ? header[i + 2] - 55 : header[i + 2] - '0');
                    i += 3;
                  } else
                    wifi_ssid[j] = header[i];
                  i++;
                  j++;
                }
                wifi_ssid[j] = '\0';
                storage->WiFi_SSID->Save(wifi_ssid);
                j = 0;
                i += 6;

                while (j < 63 && header[i] != '\0' && header[i] != ' ') {
                  if (header[i] == '%') {
                    wifi_pass[j] = (header[i + 1] - '0') * 16 + (header[i + 2] > 64 ? header[i + 2] - 55 : header[i + 2] - '0');
                    i += 2;
                  } else
                    wifi_pass[j] = header[i];
                  i++;
                  j++;
                }
                wifi_pass[j] = '\0';
                storage->WiFi_PASS->Save(wifi_pass);
                Serial.printf("SSID: '%s'\nPassword: '%s'", wifi_ssid, wifi_pass);
                Serial.println("");
                free(wifi_ssid);
                free(wifi_pass);
                header = "";
              } else if ((i = header.indexOf("GET /reboot.php")) >= 0) {
                Serial.println("Restarting...");
                WiFi.disconnect(true);
                ESP.restart();
                while (1)
                  taskYIELD();
              }
              File f = SPIFFS.open("/ap.html", "r");
              String str = f.readString();
              str.replace("%SSID%", wifi_ssid);
              client.print(str);
              break;
            } else { // if you got a newline, then clear currentLine
              currentLine = "";
            }
          } else if (c != '\r') {                   // if you got anything else but a carriage return character,
            currentLine += c; // add it to the end of the currentLine
          }
        }
      }
      taskYIELD();
    }
    client.stop();
  }
}